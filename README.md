# OpenML dataset: Tallo

https://www.openml.org/d/45081

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

TALLO - a global tree allometry and crown architecture database.

This is the Tallo dataset described in Jucker et al. (2022) but recreated with Python scripts from Laurens Bliek.
The scripts can be found at https://github.com/lbliek/TALLO_ML/tree/TALLO_ML1.

The Tallo database (v1.0.0) is a collection of 498,838 georeferenced 
and taxonomically standardized records of individual trees for which stem diameter, 
height and/or crown radius have been measured. 
Data were compiled from 61,856 globally distributed sites and include measurements for 5,163 tree species (Jucker et al., 2022).
Data was sourced from published articles between 1988 and 2021, as well as online resources: https://github.com/lbliek/TALLO_ML/blob/main/DB/Reference_look_up_table.csv

The constructed data set and associated meta-data is for use case 3 in the referenced paper: 
predicting tree height based on climate data and stem diameter.
This means a large portion of data is ignored by default (set as attributes to be ignored).

Note: Samples are taken from different sources spanning decades, multiple samples may be taken from distinct trees in the same approximate geographical location.
These relationships between samples are ignored when generating tasks on OpenML.

List with a description for each feature:

|Field|Description|
|---|---|
|tree_id|Unique tree identifier code|
|division|Major phylogenetic division (Angiosperm or Gymnosperm)|
|family|Family name|
|genus|Genus name|
|species|Species binomial name|
|latitude|Latitude (in decimal degrees)|
|longitude|Longitude (in decimal degrees)|
|stem_diameter_cm|Stem diameter (in cm). For multi-stemmed trees values for individual stems (Di) were pooled into a single value calculated as: sqrt(sum(Di^2)). Log-scaled (base 10).|
|height_m|Tree height (in m). Log-scaled (base 10).|
|crown_radius_m|Crown radius (in m)|
|height_outlier|Identifier for trees with height values flagged as outliers (Y = outlier; N = non-outlier)|
|crown_radius_outlier|Identifier for trees with crown radius values flagged as outliers (Y = outlier; N = non-outlier)|
|reference_id|Reference code corresponding to the data source from which a record was obtained (see 'Reference_look_up_table.csv' for details on data sources).|
|realm|"Biogeographic realm. Follows the classification of Olson et al. (2001) BioScience, 51, 933-938"|
|biome|"Biome type. Follows the classification of Olson et al. (2001) BioScience, 51, 933-938"|
|mean_annual_rainfall|Mean annual rainfall (in mm/yr). Values were obtained from the WorldClim2 database based on the geographic coordinates of the tree.|
|rainfall_seasonality|Rainfall seasonality (coefficent of variation). Values were obtained from the WorldClim2 database based on the geographic coordinates of the tree.|
|aridity_index|Aridity index (calculated as mean annual precipitation / potential evapotranspiration). Values were obtained from the Global Aridity Index and Potential Evapotranspiration Climate Database (v2) based on the geographic coordinates of the tree. Log-scaled (base 10).|
|mean_annual_temperature|Mean annual temperature (in degree C). Values were obtained from the WorldClim2 database based on the geographic coordinates of the tree.|
|maximum_temperature|Maximum temperature of the warmest month (in degree C). Values were obtained from the WorldClim2 database based on the geographic coordinates of the tree.|
|AT_AI| Ratio of 'mean annual temperature' over log-scaled 'aridity index'.|

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45081) of an [OpenML dataset](https://www.openml.org/d/45081). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45081/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45081/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45081/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

